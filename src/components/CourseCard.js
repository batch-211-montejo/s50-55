// import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import {Card, Button} from 'react-bootstrap';


export default function CourseCard({courseProp}) {
	
	// useState hook:
	// a hook in React is a kind of tool. The useState hook allows creation and manipulation of states
	// States are a way for React to keep track of any value and associate it with a component
	// When a state changes, React re-renders ONLY the specific component or part of the component that changed (and not the entire page or components whose states have not changed)

	// State: a special kind of variable (can be name anything) that React uses to render/re-render components when needed

	// state setter: State setters are the ONLY way to change a state's value. By convention, they arenamed after the state.

	// default state: The state's initial value on component mount
	// Before a component mounts, a state actually defaults to undefined, then is changed to its default state

	// array destructuring to get the state and the setter
	// const [count , setCount] = useState(0);

	/*
		Syntax:
		const [state, setState] = useState(default state)

	*/

	let {name, description, price, _id} = courseProp;

	// function enroll () {
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count)
	// }

	// activity
	// const [seat, seatCount] = useState(10);

	// function enroll() {
	// 	if (count !== 10) {
	// 		 setCount(count + 1);
	// 		 seatCount(seat - 1)
	// 	} 
	// }


	// Apply teh use effect hook
	// useEffect makes any given code block happen when a state changes AND when a component first mount (such as on initial page load)

	// Syntax:
		// useEffect(function , [dependencies])

	// useEffect(()=> {
	// 	if(seat === 0) {
	// 		alert("No more seats available!")
	// 	}
	// }, [count,seat])

	// alert("Sorry, no more available seat")
	return (
		<Card>
			<Card.Body className= "card-purple">
			<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>PHP {price}</Card.Text>
				<Button as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
	)        
}