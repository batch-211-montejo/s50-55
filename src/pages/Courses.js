// import coursesData from '../data/courses';
import CourseCard from '../components/CourseCard';
import { useEffect, useState } from 'react';

export default function Courses () {
	// console.log(coursesData)

	// using thr map array method, we can loop through our courseData and dynamically render any numbe rof CourseCards depending on how mnay array elements are presend in our data

	// map returns an array, which we can display in the page via the component function's return statement

	// props are a way to pass any valid JS data from parent component to child component

	// You can pass as many props as you want. Even functions can be passed as props

	const [courses, setCourses] = useState([])



	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data => {

		const courseArr = data.map(course => {
		// console.log(course)
		return (
			<CourseCard courseProp={course} key={course._id}/>
			)
		})
		setCourses(courseArr)
	})

	},[courses])


	return (
		<>
			{courses}
		</>
	)
}