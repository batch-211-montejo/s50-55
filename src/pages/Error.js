import {Button} from 'react-bootstrap';
import Banner from '../components/Banner'  //solution


export default function Error(){


    // solution
    const data = {
        title: "404 - Not Found",
        content: "The page you are looking cannot be found",
        destination: "/",
        label: "Back Home"
    }

    return(

        // "prop" name is up to the developer
        <Banner bannerProp = {data}/>




       /*<div>
            <h1>404 Error</h1>
            <h1>Page not found</h1>
            
            <Link to="/"><Button>Back to Home</Button></Link>
           
        </div>*/
    )

}