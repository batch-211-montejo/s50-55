import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'


export default function Logout(){

    // Consume the UserContext Object and destructure it to access the user state and unsetUser function from our provider.
    const { unsetUser, setUser } = useContext(UserContext);


    // clear the localStorage or user's information.
    unsetUser();

    useEffect(() => {
        // Set the user state back to its original value
        setUser({id: null});
    })




    // The "localStorage.clear()" methos allows us to clear the indormation in the localStorage ensuring no information in stored in our browser
    // In order to ee this effect we will need to refresh the browser for us to see the changes in our application
    // The proper solution to this will be discussed in the next session.
    // Closing and reopening browser and even restarting our devices will make the information in the localStorage persistent
    // It's always good practice to include code in our application to clear the localStorage when it is no longer needed.
    // localStorage.clear()

    return(
        <Navigate to="/login"/>
        )

}